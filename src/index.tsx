import React from 'react';
import ReactDOM from 'react-dom';
//import { makeStyles } from '@material-ui/core/styles';
import { useChartWidget } from 'widget';

// const useStyles = makeStyles({
//   '@global': {
//     html: {
//       height: '100%',
//     },
//     body: {
//       height: '100%',
//       margin: 0,
//     },
//     '#root': {
//       height: '100%',
//       display: 'flex',
//     },
//   },
//   root: {
//     flex: 1,
//     display: 'flex',
//     flexDirection: 'column',
//   },
//   chartContainer: {
//     flex: 2,
//     display: 'flex',
//     flexDirection: 'column',
//     '&>*': {
//       flex: 1,
//     },
//   },
// });

function App() {
  // const classes = useStyles();


  useChartWidget();

  return (
    <div id={"root"}>
      <div id="tv-widget" style={ {width:"100vw", height:"100vh"} } />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
