import { useState, useEffect, useCallback } from 'react';

import { createWidget } from './createWidget';
import { Widget } from './Widget';

export function useChartWidget() {
  const [widget, setWidget] = useState<Widget | null>(null);
  const [initialized, setInitialized] = useState(false);
  const [sharingChart, setSharingChart] = useState(false);

  const handleShareChart = useCallback(() => setSharingChart(true), []);
  const stopSharingChart = useCallback(() => setSharingChart(false), []);

  const [addingIndicator, setAddingIndicator] = useState(false);

  const stopAddingIndicator = useCallback(() => setAddingIndicator(false), []);

  useEffect(() => {
    if (!initialized) {
      // Widget must be a singleton per component, so we initialize it only once

      const initializeChartWidget = async () => {
        const widget = await createWidget();

        setWidget(widget);
      };

      initializeChartWidget();

      setInitialized(true);
    }
  }, [handleShareChart, initialized]);

  return {
    widget,
    sharingChart,
    stopSharingChart,
    addingIndicator,
    stopAddingIndicator,
  };
}
