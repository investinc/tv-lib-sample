import { widget as TradingViewWidget } from '../tvlib';

import { Datafeed } from './Datafeed';
import { Widget } from './Widget';

export async function createWidget() {
  const datafeed = new Datafeed();

  const chartWidgetAPI = new TradingViewWidget({
    datafeed: datafeed as any,
    symbol: 'Binance:BTC/USDT',
    container: 'tv-widget',
    library_path: '/assets/tvlib/',
    interval: '240' as any,
    locale: 'en',
    debug: process.env.NODE_ENV === 'development',
    fullscreen: false,
    theme: 'Dark',
    autosize: true,
    auto_save_delay: 0.5,
    time_frames: [
      { text: '5y', resolution: 'W' },
      { text: '1y', resolution: 'D' },
      { text: '6m', resolution: '240' },
      { text: '3m', resolution: '60' },
      { text: '1m', resolution: '30' },
      { text: '5d', resolution: '15' },
      { text: '1d', resolution: '1' },
    ] as any,
  });

  // we must wait for the chart to be ready before starting to subscribe on it's events
  await new Promise(resolve => chartWidgetAPI.onChartReady(resolve as any));

  // The Widget class adds buttons, so the header must be ready before we instantiate the class
  await chartWidgetAPI.headerReady();

  const widget = new Widget(chartWidgetAPI);

  return widget;
}
