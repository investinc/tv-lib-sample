import clamp from 'lodash/clamp';

import {  OHLCVLiveAggregator, ohlcvTimeUnitFromString, ohlcvTimeUnitToString } from '../ohlcvHelpers';
import {
  IDatafeedChartApi,
  OnReadyCallback,
  ResolveCallback,
  LibrarySymbolInfo,
  SearchSymbolsCallback,
  ErrorCallback,
  HistoryCallback,
  SubscribeBarsCallback,
  PeriodParams,
} from '../tvlib';

import { getHistoricalOHLCVItems, getLatestOHLCVItems, supportedTimeUnits } from './ohlcv';
import { subscribeToOHLCVUpdate } from './ohlcvStream';

const supportedResolutions = supportedTimeUnits.map(timeUnit => ohlcvTimeUnitToString(timeUnit, 'trading-view') as any);

const BINANCE_BTC_USDT_PAIR_ID = 324;

export class Datafeed implements IDatafeedChartApi {
  private _unsubscribeMap = new Map<string, () => void>();

  onReady(callback: OnReadyCallback) {
    setTimeout(() =>
      callback({
        supported_resolutions: supportedResolutions,
      }),
    );
  }

  searchSymbols(userInput: string, exchange: string, symbolType: string, onResult: SearchSymbolsCallback) {
    setTimeout(() => onResult([]));
  }

  resolveSymbol(symbolName: string, onResolve: ResolveCallback, onError: ErrorCallback) {
    setTimeout(async () => {
      onResolve({
        ticker: symbolName,
        name: symbolName,
        full_name: symbolName,
        exchange: 'NASDAQ',
        listed_exchange: 'NASDAQ',
        description: 'Tesla, Inc',
        format: 'price',
        type: 'stock',
        session: '0900-1600',
        timezone: 'America/New_York',
        minmov: 1,
        pricescale: 100,
        volume_precision: 8,
        has_intraday: true,
        has_daily: true,
        has_weekly_and_monthly: true,
        has_empty_bars: true,
        supported_resolutions: supportedResolutions,
        data_status: 'streaming',
      });
    });
  }

  async getBars(
    symbolInfo: LibrarySymbolInfo,
    resolution: string,
    { from, to }: PeriodParams,
    onResult: HistoryCallback,
    onError: ErrorCallback,
  ) {
    const timeUnit = ohlcvTimeUnitFromString(resolution);

    if (!timeUnit) {
      onError('Invalid Resolution');

      return
    }

    const start = clamp(from, 0, Infinity);
    const end = clamp(to, 0, Infinity);

    try {
      const { data, olderDataAvailable } = await getHistoricalOHLCVItems(start * 1000, end * 1000, BINANCE_BTC_USDT_PAIR_ID, timeUnit);

      const bars = data.map(({ timeUnitStart, timeClose, timeOpen, ...rest }) => ({ time: timeUnitStart, ...rest }));

      console.log(bars)

      if (bars.length > 0) {
        onResult(bars, { noData: false });
      } else {
        onResult([], { noData: !olderDataAvailable });
      }
    } catch (error) {
      console.error(error);

      onError((error as any).message);
    }
  }

  subscribeBars(symbol: LibrarySymbolInfo, resolution: string, onTick: SubscribeBarsCallback, uuid: string, resetCache: () => void): void {
    const timeUnit = ohlcvTimeUnitFromString(resolution);

    if (timeUnit === null) {
      return;
    }

    const aggregator = new OHLCVLiveAggregator({
      enableHistory: true,
      timeUnit,
      retention: { method: 'record-limit', retentionCount: 5 },
    });

    const recentHistoryPromise = getLatestOHLCVItems(BINANCE_BTC_USDT_PAIR_ID, timeUnit,  2 );

    recentHistoryPromise.then(({ data }) => {
      aggregator.setHistory(data);

      const aggregated = aggregator.getMostRecentOHLCVItem();

      if (aggregated) {
        const { timeUnitStart, open, high, close, low, volume } = aggregated;

        onTick({ time: timeUnitStart, open, high, close, low, volume });
      }
    });

    const unsubscribeOHLCVUpdates = subscribeToOHLCVUpdate(BINANCE_BTC_USDT_PAIR_ID, update => {
      console.log('update callback')
      const aggregated = aggregator.applyUpdate(update);

      if (aggregated) {
        const { timeUnitStart, open, high, close, low, volume } = aggregated;

        onTick({ time: timeUnitStart, open, high, close, low, volume });
      }
    });
    
    this._unsubscribeMap.set(uuid, unsubscribeOHLCVUpdates);
  }

  unsubscribeBars(uuid: string) {
    this._unsubscribeMap.get(uuid)?.();

    this._unsubscribeMap.delete(uuid);
  }
}
