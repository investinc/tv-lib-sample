import axios from 'axios';

import { getTimeUnitDuration, OHLCVDataItem, OHLCVTimeUnit, ohlcvTimeUnitToString, startOfTimeUnit } from '../ohlcvHelpers';

// export const AUROX_OHLCV_HISTORY_SERVICE_ENDPOINT = 'https://ohlcv-history.getaurox.com/api/v1/ohlcv';
// export const SERVICE_SLOG = 'history';

export const AUROX_OHLCV_HISTORY_SERVICE_ENDPOINT = 'http://localhost:8080/api/v1/ice/history/ohlcv';
export const SERVICE_SLOG = '/';

export const supportedTimeUnits: OHLCVTimeUnit[] = [

  { granularity: 'minute', period: 1 },
  { granularity: 'minute', period: 2 },
  { granularity: 'minute', period: 3 },
  { granularity: 'minute', period: 4 },
  { granularity: 'minute', period: 5 },
  { granularity: 'minute', period: 6 },
  { granularity: 'minute', period: 10 },
  { granularity: 'minute', period: 15 },
  { granularity: 'minute', period: 20 },
  { granularity: 'minute', period: 30 },

  { granularity: 'hour', period: 1 },
  { granularity: 'hour', period: 2 },
  { granularity: 'hour', period: 3 },
  { granularity: 'hour', period: 4 },
  { granularity: 'hour', period: 6 },
  { granularity: 'hour', period: 8 },
  { granularity: 'hour', period: 12 },

  { granularity: 'day', period: 1 },
  { granularity: 'day', period: 2 },
  { granularity: 'day', period: 3 },
  { granularity: 'day', period: 5 },
  { granularity: 'day', period: 10 },

  { granularity: 'week', period: 1 },

  { granularity: 'month', period: 1 },
];

export interface ConvertFromCompactOHLCVDataResult {
  rows: OHLCVDataItem[];
  leastRetainedTimeUnitStart: number;
  olderDataAvailable: boolean;
}

export function convertFromCompactOHLCVData(body: any): ConvertFromCompactOHLCVDataResult {
  const { data, columns } = body;

  const result: OHLCVDataItem[] = [];

  for (const item of data) {
    const resultItem: any = {};

    for (let i = 0; i < columns.length; i++) {
      resultItem[columns[i]] = item[i];
    }

    result.push(resultItem);
  }

  result.reverse();

  return {
    rows: result,
    leastRetainedTimeUnitStart: body.leastRetainedTimeUnitStart,
    olderDataAvailable: body.olderDataAvailable,
  };
}

export async function getHistoricalOHLCVItems(startTime: number, endTime: number, pairId: number, timeUnit: OHLCVTimeUnit) {
  const response = await axios.get(SERVICE_SLOG, {
    baseURL: AUROX_OHLCV_HISTORY_SERVICE_ENDPOINT,
    params: {
      pair_id: pairId,
      time_start: startTime <= 0 ? 1000 : startTime,
      time_end: endTime <= 0 ? 1000 : endTime,
      time_unit: ohlcvTimeUnitToString(timeUnit),
    },
  });

  return { data: convertFromCompactOHLCVData(response.data).rows.reverse(), olderDataAvailable: !!response.data.olderDataAvailable };
}

export async function getLatestOHLCVItems(pairId: number, timeUnit: OHLCVTimeUnit, limit: number) {
  const startTime = startOfTimeUnit(Date.now() - getTimeUnitDuration(timeUnit) * limit, timeUnit);

  const response = await axios.get('latest', {
    baseURL: AUROX_OHLCV_HISTORY_SERVICE_ENDPOINT,
    params: {
      pair_id: pairId,
      time_start: startTime <= 0 ? 0 : startTime,
      time_unit: ohlcvTimeUnitToString(timeUnit),
    },
  });

  return { data: convertFromCompactOHLCVData(response.data).rows.reverse(), olderDataAvailable: !!response.data.olderDataAvailable };
}
