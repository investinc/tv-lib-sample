import noop from 'lodash/noop';

import { IChartingLibraryWidget } from '../tvlib';

export class Widget {
  public chartWidgetAPI: IChartingLibraryWidget;
  public chartFrame: HTMLIFrameElement;

  public hideTradeLines = false;
  public showExecutionShapes: boolean | 'combined' = false;

  public get chartAPI() {
    return this.chartWidgetAPI.chart();
  }

  public get interval() {
    return this.chartWidgetAPI.symbolInterval().interval;
  }

  public get symbol() {
    return this.chartWidgetAPI.symbolInterval().symbol;
  }

  public get chartContentWindow(): Window {
    return this.chartFrame.contentWindow!;
  }

  public get chartContentDocument(): HTMLDocument {
    return this.chartFrame.contentDocument!;
  }

  public get chartWidget() {
    return (this.chartContentWindow as any).chartWidget;
  }

  public get mainPaneWidget() {
    return this.chartWidget._getMainSeriesPaneWidget();
  }

  public get mainPaneLegendWidget() {
    return this.mainPaneWidget._legendWidget;
  }

  public get mainPaneRoot(): HTMLDivElement {
    return this.mainPaneWidget._div;
  }

  public get mainPaneCanvas(): HTMLCanvasElement {
    return this.mainPaneWidget._canvas;
  }

  public get allStudyDescriptors() {
    return this.chartAPI.getAllStudies();
  }

  public get barSpacing(): number {
    return this.chartWidget.model().timeScale().barSpacing();
  }

  public get chartDataReady(): boolean {
    return this.chartAPI.dataReady(noop);
  }

  constructor(chartWidgetAPI: IChartingLibraryWidget) {
    this.chartWidgetAPI = chartWidgetAPI;
    this.chartFrame = (chartWidgetAPI as any)._iFrame;
  }

  public getMainSeriesBars = (): any[] => {
    return this.chartWidget._model.mainSeries().bars()._items;
  };

  public getStudyFromId = (id: string) => {
    return this.chartWidget.model().model().getStudyById(id);
  };

  public getViewingTimeAndPriceRanges = () => {
    const priceRange = this.chartAPI
      .getPanes()
      .find(pane => pane.hasMainSeries())
      ?.getMainSourcePriceScale()
      ?.getVisiblePriceRange();

    const timeRange = this.chartAPI.getVisibleRange();

    return {
      priceRange: priceRange ? ([priceRange.from, priceRange.to] as [number, number]) : null,
      timeRange: timeRange ? ([timeRange.from, timeRange.to] as [number, number]) : null,
    };
  };

  public getMainPanePriceRange = (): [number, number] | null => {
    try {
      const priceScale = this.mainPaneWidget.state().mainDataSource().priceScale();

      const mode = priceScale.mode();

      if (mode.indexedTo100 || mode.log || mode.percentage) {
        return null;
      }

      return [priceScale.coordinateToPrice(priceScale.height()), priceScale.coordinateToPrice(0)];
    } catch {
      return null;
    }
  };
}
