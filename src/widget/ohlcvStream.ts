import PersistentWebsocketConnection from '../pws';
import { OHLCVDataItem } from '../ohlcvHelpers';

import { Client, Message } from '@stomp/stompjs';
// import StompJs from '@stomp/stompjs'
// import SockJS from 'sockjs-client';
// import {Stomp} from '@types/stompjs';

// export const AUROX_OHLCV_STREAMER_URL = 'wss://streamer-ohlcv.getaurox.com';
export const AUROX_OHLCV_STREAMER_URL = 'ws://localhost:8080/stream/v1'

export interface OHLCVServiceConnectionOptions {
  getCurrentPairIds: () => number[];
  onUpdate: (pairId: number, update: OHLCVDataItem) => void;
}

export class OHLCVServiceConnection {
  private connection: PersistentWebsocketConnection;
  private onUpdate: (pairId: number, update: OHLCVDataItem) => void;

  public get isConnected() {
    console.log('is connected !!')
    return this.connection.isConnected;
  }

  public get isConnecting() {
    return this.connection.isConnecting;
  }

  constructor(options: OHLCVServiceConnectionOptions) {
    this.onUpdate = options.onUpdate;

    function handleConnectionEstablished(send: (data: any) => void) {
      const pairIds = options.getCurrentPairIds();

      if (pairIds.length > 0) {
        //send(JSON.stringify({ action: 'subscribe', pairIds }));

        // var subscribe = "SUBSCRIBE\n" + 
        //                 "destination: /topic/quotes\n" + 
        //                 "ack: client\n\n" +
        //                 "^@"

        // var sendMsg = "SEND\n" +
        //               "destination:/stream/quotes\n" +
        //               "receipt:message-12345\n\n" +
        //               "Hello a!^@"
                  
        // // send(JSON.stringify({ action: 'subscribe', pairIds }));
        // // console.log(subscribe)
        // console.log(sendMsg)
        
        // send(subscribe)
        // send(sendMsg)
      }
    }

    this.connection = new PersistentWebsocketConnection({
      url: AUROX_OHLCV_STREAMER_URL,
      onData: this.handleData,
      onReconnect: handleConnectionEstablished,
      onInitialConnectionEstablished: handleConnectionEstablished,
      pingPong: { enabled: true, heartbeatDebounce: 5000, toleranceTimeout: 4000 },
      keepAlive: { enabled: true, interval: 5000 },
    });
  }

  private handleData = (data: any) => {
    console.log('handle data')
    const [update, ...rest] = String(data).split('\n');

    if (update === 'error') {
      console.error('OHLCV streamer returned an error: ', rest.join('\n'));

      return;
    }

    const [pairIdStr, timeUnitStartStr, timeOpenStr, timeCloseStr, openStr, highStr, lowStr, closeStr, volumeStr] = update.split(',');

    this.onUpdate(Number(pairIdStr), {
      timeUnitStart: Number(timeUnitStartStr),
      timeOpen: Number(timeOpenStr),
      timeClose: Number(timeCloseStr),
      open: Number(openStr),
      high: Number(highStr),
      close: Number(closeStr),
      low: Number(lowStr),
      volume: Number(volumeStr),
    });
  };

  public applyAction = (action: 'subscribe' | 'unsubscribe', pairIds: number[]) => {
    if (this.connection.isConnected) {
      this.connection.send(JSON.stringify({ action, pairIds }));
    }
  };

  public stop() {
    this.connection.stop();
  }
}


// export function subscribeToOHLCVUpdate(pairId: number, callback: (update: OHLCVDataItem) => void) {
//   console.log('subscribeToOHLCVUpdate', pairId)
//   const connection = new OHLCVServiceConnection({
//     getCurrentPairIds: () => [pairId],
//     onUpdate: (pairId, data) => callback(data),
//   });

//   return () => {
//     connection.stop();
//   };
// }

export function subscribeToOHLCVUpdate(pairId: number, callback: (update: OHLCVDataItem) => void) {

  // let socket = new SockJS('http://localhost:8080/stream/v1');
  // let stompClient = Stomp.over(socket);

//   const client = new StompJs.Client();

//   stompClient.connect({}, function () {
//     // setConnected(true);
//     console.log('Connected: ');
//     stompClient.subscribe('/topic/quotes', function (greeting) {
//         // showGreeting(JSON.parse(greeting.body).content);
//     });
// });

  const client = new Client({
    brokerURL: AUROX_OHLCV_STREAMER_URL,
    // connectHeaders: {
    //   login: 'user',
    //   passcode: 'password',
    // },
    debug: function (str) {
      console.log(str);
    },
    reconnectDelay: 5000,
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000,
  });

  client.onConnect = function (frame) {
    console.log('conntected... subscribe')

    client.subscribe('/topic/quotes', function (greeting) {
      console.log('subscribed!', greeting)
    });

    // Do something, all subscribes must be done is this callback
    // This is needed because this will be executed after a (re)connect
  };

  client.onStompError = function (frame) {
    // Will be invoked in case of error encountered at Broker
    // Bad login/passcode typically will cause an error
    // Complaint brokers will set `message` header with a brief message. Body may contain details.
    // Compliant brokers will terminate the connection after any error
    console.log('Broker reported error: ' + frame.headers['message']);
    console.log('Additional details: ' + frame.body);
  };

  client.activate();

  return () => {
        // connection.stop();
      };
}



