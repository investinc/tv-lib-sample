import { PersistentWebsocketConnectionOptions } from './types';
import {
  DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_RECONNECT_TIMEOUT,
  DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_CONNECTION_CHECK_INTERVAL,
  DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_PING_PONG_INTERVAL,
  DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_PING_PONG_TOLERANCE_TIMEOUT,
  DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_KEEP_ALIVE_INTERVAL,
} from './constants';

function defaultDoPing(send: (data: any) => void) {
  send('ping');
}

function defaultDoCheckPong(event: MessageEvent) {
  return event.data === 'pong';
}

function defaultDoCheckHeartbeat() {
  return false;
}

function defaultDoKeepAlive(send: (data: any) => void) {
  send('ping');
}

function noop() {
  return;
}

type DeepRequired<T> = T extends Function ? T : T extends object ? { [P in keyof T]-?: DeepRequired<T[P]> } : Required<T>;

export function getOptionsWithDefaults(options: PersistentWebsocketConnectionOptions): DeepRequired<PersistentWebsocketConnectionOptions> {
  return {
    url: options.url,
    reconnectTimeout: options.reconnectTimeout || DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_RECONNECT_TIMEOUT,
    reconnectBackoff: {
      enabled: (options.reconnectBackoff && options.reconnectBackoff.enabled) || false,
      backoffOffset: (options.reconnectBackoff && options.reconnectBackoff.backoffOffset) || 1,
      backoffIncrement: (options.reconnectBackoff && options.reconnectBackoff.backoffIncrement) || DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_RECONNECT_TIMEOUT,
      maxBackedOffTimeout: (options.reconnectBackoff && options.reconnectBackoff.maxBackedOffTimeout) || (DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_RECONNECT_TIMEOUT * 10),
      backoffIncrementResetTimeout: (options.reconnectBackoff && options.reconnectBackoff.backoffIncrementResetTimeout) || (DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_RECONNECT_TIMEOUT * 10),
    },
    connectionCheckInterval: options.connectionCheckInterval || DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_CONNECTION_CHECK_INTERVAL,
    pingPong: {
      enabled: (options.pingPong && options.pingPong.enabled) || false,
      heartbeatOnly: (options.pingPong && options.pingPong.heartbeatOnly) || false,
      disableDebounceOnMessage: (options.pingPong && options.pingPong.disableDebounceOnMessage) || false,
      forwardPongMessage: (options.pingPong && options.pingPong.forwardPongMessage) || false,
      forwardHeartbeatMessage: (options.pingPong && options.pingPong.forwardHeartbeatMessage) || false,
      interval: (options.pingPong && options.pingPong.interval) || DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_PING_PONG_INTERVAL,
      toleranceTimeout: (options.pingPong && options.pingPong.toleranceTimeout) || DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_PING_PONG_TOLERANCE_TIMEOUT,
      heartbeatDebounce: (options.pingPong && options.pingPong.heartbeatDebounce) || null,
      doPing: (options.pingPong && options.pingPong.doPing) || defaultDoPing,
      doCheckPong: (options.pingPong && options.pingPong.doCheckPong) || defaultDoCheckPong,
      doCheckHeartbeat: (options.pingPong && options.pingPong.doCheckHeartbeat) || defaultDoCheckHeartbeat,
    },
    keepAlive: {
      enabled: (options.keepAlive && options.keepAlive.enabled) || false,
      interval: (options.keepAlive && options.keepAlive.interval) || DEFAULT_PERSISTENT_WEBSOCKET_CONNECTION_KEEP_ALIVE_INTERVAL,
      doKeepAlive: (options.keepAlive && options.keepAlive.doKeepAlive) || defaultDoKeepAlive,
    },
    logger: {
      debug: (options.logger && options.logger.debug) || noop,
      error: (options.logger && options.logger.error) || noop,
    },
    onInitialConnectionEstablished: options.onInitialConnectionEstablished,
    onReconnect: options.onReconnect,
    onData: options.onData,
    onReconnecting: options.onReconnecting || noop,
    onConnecting: options.onConnecting || noop,
    onConnectionLost: options.onConnectionLost || noop,
  };
}
