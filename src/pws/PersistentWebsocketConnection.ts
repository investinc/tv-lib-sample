import { TypedEmitter } from 'tiny-typed-emitter';

import { getOptionsWithDefaults } from './defaults';
import {
  PersistentWebsocketConnectionPingPongOptions,
  PersistentWebsocketConnectionReconnectionBackoff,
  PersistentWebsocketConnectionOptions,
  PersistentWebsocketConnectionLogger,
  ConnectionLostEvent,
  ConnectionLostReason,
  PersistentWebsocketConnectionKeepAlive,
} from './types';

export interface PersistentWebsocketConnectionEvents {
  'connecting': () => void;
  'connected': () => void;
  'pong': () => void;
  'heartbeat': () => void;
  'connection-lost': (reason: ConnectionLostReason, error?: string) => void;
  'reconnecting': (attempt: number) => void;
  'reconnected': () => void;
  'data': (data: string) => void;
  'stopped': () => void;
}

export class PersistentWebsocketConnection extends TypedEmitter<PersistentWebsocketConnectionEvents> {
  private connection: WebSocket | null = null;

  private logger: PersistentWebsocketConnectionLogger;

  private url: string;
  private reconnectTimeout: number;
  private connectionCheckInterval: number;

  private pingPongOptions: PersistentWebsocketConnectionPingPongOptions;
  private keepAliveOptions: PersistentWebsocketConnectionKeepAlive;
  private backoffOptions: PersistentWebsocketConnectionReconnectionBackoff;

  private attemptingToEstablishConnection = false;
  private closedExternally = false;
  private closedDuoToPongOrHeartbeatTimeout = false;
  private initialConnectionEstablished = false;
  private reconnecting = false;
  private reconnectingForcefully = false;
  private reconnectingTries = 0;
  private reconnectBackoffIncrement = 0;

  private onInitialConnectionEstablished: (send: (data: any) => void) => void;
  private onReconnect: (send: (data: any) => void) => void;
  private onData: (data: string) => void;
  private onReconnecting: (tries: number) => void;
  private onConnecting: (connection: WebSocket) => void;
  private onConnectionLost: (event: ConnectionLostEvent) => void;

  private queuedDataToSendOnReconnect: any[] = [];

  private reconnectTimer: any = null;
  private reconnectDelayTimer: any = null;
  private reconnectBackoffIncrementResetTimer: any = null;

  private checkConnectionTimer: any = null;
  private pingPongTimer: any = null;
  private pingPongToleranceTimer: any = null;

  private keepAliveTimer: any = null;

  public get isConnected() {
    return this.connection?.readyState === this.connection?.OPEN;
  }

  public get isConnecting() {
    return this.connection?.readyState === this.connection?.CONNECTING;
  }

  constructor(options: PersistentWebsocketConnectionOptions) {
    super();

    const optionsWithDefaults = getOptionsWithDefaults(options);

    this.logger = optionsWithDefaults.logger;
    this.url = optionsWithDefaults.url;
    this.reconnectTimeout = optionsWithDefaults.reconnectTimeout;
    this.connectionCheckInterval = optionsWithDefaults.connectionCheckInterval;
    this.pingPongOptions = optionsWithDefaults.pingPong;
    this.keepAliveOptions = optionsWithDefaults.keepAlive;
    this.backoffOptions = optionsWithDefaults.reconnectBackoff;
    this.onInitialConnectionEstablished = optionsWithDefaults.onInitialConnectionEstablished;
    this.onReconnect = optionsWithDefaults.onReconnect;
    this.onData = optionsWithDefaults.onData;
    this.onReconnecting = optionsWithDefaults.onReconnecting;
    this.onConnecting = optionsWithDefaults.onConnecting;
    this.onConnectionLost = optionsWithDefaults.onConnectionLost;

    this.connect();
    this.checkConnection();
  }

  public send = (data: any) => {
    if (this.isConnected) {
      this.connection?.send(data);
    }
  }

  public sendOrQueue = (data: any) => {
    if (this.isConnected) {
      this.connection?.send(data);
    } else {
      this.queuedDataToSendOnReconnect.push(data);
    }
  }

  public stop = () => {
    if (this.closedExternally) {
      return;
    }

    clearTimeout(this.reconnectTimer);
    clearTimeout(this.reconnectDelayTimer);
    clearTimeout(this.reconnectBackoffIncrementResetTimer);
    clearTimeout(this.checkConnectionTimer);
    clearTimeout(this.pingPongTimer);
    clearTimeout(this.pingPongToleranceTimer);

    clearTimeout(this.keepAliveTimer);

    this.closedExternally = true;

    this.closeConnection();

    this.logger.debug(`Connection to ${this.url} closed gracefully.`);

    this.emit('stopped');
  }

  public forceReconnect = () => {
    if (this.closedExternally) {
      return;
    }

    this.logger.debug(`Attempting to forcefully reconnect to ${this.url}.`);

    clearTimeout(this.pingPongTimer);
    clearTimeout(this.pingPongToleranceTimer);

    clearTimeout(this.keepAliveTimer);

    // When forcefully reconnecting, we must reset the backoff state since it implies an urgent requirement
    this.resetBackoffIncrement();

    this.reconnectingForcefully = true;

    this.closeConnection();
  }

  private resetBackoffIncrement = () => {
    this.reconnectBackoffIncrement = 0;
  }

  private reconnect = () => {
    if (this.closedExternally || this.isConnected) {
      this.reconnectingTries = 0;
      this.reconnecting = false;
      return;
    }

    this.reconnecting = true;

    this.reconnectingTries += 1;

    this.onReconnecting(this.reconnectingTries);

    this.emit('reconnecting', this.reconnectingTries);

    if (!this.attemptingToEstablishConnection) {
      this.logger.debug(`Attempting to reconnect to ${this.url}.`);
      this.connect();
    } else {
      this.logger.debug(`Attempted to reconnect to ${this.url}, but the previous attempt is still unresolved.`);
    }

    clearTimeout(this.reconnectTimer);
    this.reconnectTimer = setTimeout(() => {
      if (this.closedExternally || this.isConnected) {
        this.reconnectingTries = 0;
        this.reconnecting = false;
        return;
      }

      this.logger.debug(`Reconnection to ${this.url} timed out after ${this.reconnectTimeout}ms, retrying...`);

      this.reconnectWithDelay();
    }, this.reconnectTimeout);
  }

  private reconnectWithDelay = () => {
    clearTimeout(this.reconnectTimer);
    clearTimeout(this.reconnectDelayTimer);
    clearTimeout(this.reconnectBackoffIncrementResetTimer);

    if (this.closedExternally || this.isConnected) {
      this.reconnectingTries = 0;
      this.reconnecting = false;
      return;
    }

    this.reconnecting = true;

    let delay = 0;

    if (this.backoffOptions.enabled) {
      this.reconnectBackoffIncrement += 1;

      const incrementDiff = this.reconnectBackoffIncrement - this.backoffOptions.backoffOffset;
      const increment = incrementDiff <= 0 ? 0 : Math.round(incrementDiff);

      delay = this.backoffOptions.backoffIncrement * increment;

      if (delay > this.backoffOptions.maxBackedOffTimeout) {
        delay = this.backoffOptions.maxBackedOffTimeout;
      }

      if (delay > 0) {
        this.logger.debug(`Will try to reconnect to ${this.url} after ${delay}ms.`);

        clearTimeout(this.reconnectDelayTimer);
        this.reconnectDelayTimer = setTimeout(this.reconnect, delay);

        return;
      }
    }

    this.reconnect();
  }

  private closeConnection = () => {
    try {
      this.connection?.close();
    } catch {
      // Ignore errors here, failure to close is not very problematic
    }
  }

  private handleConnectionLost = (reason: ConnectionLostReason, error?: string) => {
    let preventDefault = false;

    console.log(reason)

    this.onConnectionLost({
      reason,
      error,
      preventDefault: () => {
        preventDefault = true;
      },
    });


    if (!preventDefault) {
      this.emit('connection-lost', reason, error);

      this.reconnectWithDelay();
    }
  }

  private handleNoPong = () => {
    if (this.pingPongOptions.heartbeatOnly) {
      const debounce = this.pingPongOptions.heartbeatDebounce ? this.pingPongOptions.heartbeatDebounce : 0;
      const tolerance = this.pingPongOptions.toleranceTimeout + debounce;

      this.logger.error(`Connection to ${this.url}: Heartbeat was not received for ${tolerance}ms, it will be reset.`);
    } else {
      const tolerance = this.pingPongOptions.toleranceTimeout;

      this.logger.error(`Connection to ${this.url}: Ping was not answered by a pong for ${tolerance}ms, it will be reset.`);
    }

    clearTimeout(this.pingPongTimer);

    this.closedDuoToPongOrHeartbeatTimeout = true;

    this.closeConnection();
  }

  private ping = (intervalOverride?: number) => {
    this.pingPongTimer = setTimeout(() => {
      if (this.isConnected) {
        this.pingPongToleranceTimer = setTimeout(this.handleNoPong, this.pingPongOptions.toleranceTimeout);

        if (!this.pingPongOptions.heartbeatOnly) {
          this.pingPongOptions.doPing(this.send);
        }
      }
    }, intervalOverride || this.pingPongOptions.interval);
  }

  private keepAlive = () => {
    this.keepAliveTimer = setTimeout(() => {
      if (this.isConnected) {
        this.keepAliveOptions.doKeepAlive(this.send);

        this.keepAlive();
      }
    }, this.keepAliveOptions.interval);
  }

  private handleOpen = () => {
    console.log('handleOpen')
    this.attemptingToEstablishConnection = false;
    this.reconnectingForcefully = false;
    this.closedDuoToPongOrHeartbeatTimeout = false;

    

    this.emit('connected');

    if (!this.initialConnectionEstablished) {
      this.logger.debug(`Connection to ${this.url} was successfully established.`);
      this.onInitialConnectionEstablished(this.send);
      this.initialConnectionEstablished = true;
    } else {
      clearTimeout(this.reconnectBackoffIncrementResetTimer);
      this.reconnectBackoffIncrementResetTimer = setTimeout(this.resetBackoffIncrement, this.backoffOptions.backoffIncrementResetTimeout);

      this.logger.debug(`Successfully reconnected to ${this.url}.`);
      this.onReconnect(this.send);
      this.emit('reconnected');
    }

    if (this.queuedDataToSendOnReconnect.length > 0) {
      const queue = this.queuedDataToSendOnReconnect;
      this.queuedDataToSendOnReconnect = [];

      this.logger.debug(`Attempting to send ${queue.length} queued item(s) to ${this.url}.`);

      for (const data of queue) {
        this.connection?.send(data);
      }
    }

    if (this.pingPongOptions.enabled) {
      this.ping();
    }

    if (this.keepAliveOptions.enabled) {
      this.keepAlive();
    }
  }

  private handleClose = () => {
    console.log('handleClose')
    this.attemptingToEstablishConnection = false;

    clearTimeout(this.pingPongTimer);
    clearTimeout(this.pingPongToleranceTimer);

    clearTimeout(this.keepAliveTimer);

    if (!this.closedExternally && !this.reconnecting) {
      if (this.closedDuoToPongOrHeartbeatTimeout) {
        this.closedDuoToPongOrHeartbeatTimeout = false;

        this.handleConnectionLost('pong-or-heartbeat-timeout');
      } else {
        this.handleConnectionLost('closed-unexpectedly');
      }
    }
  }

  private handleError = (event: Event) => {
    console.log('handle err', event)
    
    this.attemptingToEstablishConnection = false;

    clearTimeout(this.pingPongTimer);
    clearTimeout(this.pingPongToleranceTimer);

    clearTimeout(this.keepAliveTimer);

    if (!this.closedExternally && !this.reconnecting) {
      this.logger.error(`Connection to ${this.url} encountered an error.`);

      this.handleConnectionLost('error', (event && (event as any).message) || undefined);
    }
  }

  private handleMessage = (event: MessageEvent) => {
    console.log('handleMessage')
    if (this.pingPongOptions.enabled) {
      if (this.pingPongOptions.doCheckHeartbeat(event)) {
        this.emit('heartbeat');

        clearTimeout(this.pingPongTimer);
        clearTimeout(this.pingPongToleranceTimer);

        if (this.pingPongOptions.heartbeatDebounce !== null) {
          this.ping(this.pingPongOptions.heartbeatDebounce);
        } else {
          this.ping();
        }

        if (!this.pingPongOptions.forwardHeartbeatMessage) {
          return;
        }
      } else if (!this.pingPongOptions.heartbeatOnly && this.pingPongOptions.doCheckPong(event)) {
        this.emit('pong');

        clearTimeout(this.pingPongTimer);
        clearTimeout(this.pingPongToleranceTimer);

        this.ping();

        if (!this.pingPongOptions.forwardPongMessage) {
          return;
        }
      } else if (!this.pingPongOptions.disableDebounceOnMessage) {
        clearTimeout(this.pingPongTimer);
        clearTimeout(this.pingPongToleranceTimer);

        this.ping();
      }
    }

    const data = event.data === 'string' ? event.data : (event.data as Buffer).toString();

    this.onData(data);
    this.emit('data', data);
  }

  private checkConnection = () => {
    if (this.closedExternally) {
      return;
    }

    if (this.initialConnectionEstablished && !this.reconnecting && !this.isConnected) {
      if (this.reconnectingForcefully) {
        this.logger.debug(`Connection to ${this.url} was forcefully closed.`);
      } else {
        this.logger.error(`Connection to ${this.url} was unexpectedly lost.`);
      }

      clearTimeout(this.pingPongTimer);
      clearTimeout(this.pingPongToleranceTimer);

      clearTimeout(this.keepAliveTimer);

      if (this.closedDuoToPongOrHeartbeatTimeout) {
        this.closedDuoToPongOrHeartbeatTimeout = false;

        this.handleConnectionLost('pong-or-heartbeat-timeout');
      } else {
        this.handleConnectionLost('closed-unexpectedly');
      }
    }

    this.checkConnectionTimer = setTimeout(this.checkConnection, this.connectionCheckInterval);
  }

  private connect = () => {
    this.closeConnection();

    this.attemptingToEstablishConnection = true;

    console.log(`Attempting to connect to ${this.url}.`);

    console.log('bef connect...')
    this.connection = new WebSocket(this.url);
    console.log('after connect...')

    this.onConnecting(this.connection);
    this.emit('connecting');

    this.connection.onopen = this.handleOpen;
    this.connection.onmessage = this.handleMessage;
    this.connection.onclose = this.handleClose;
    this.connection.onerror = this.handleError;
  }
}
