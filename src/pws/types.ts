export interface PersistentWebsocketConnectionPingPongOptions {
  enabled: boolean;
  heartbeatOnly: boolean;
  disableDebounceOnMessage: boolean;
  forwardPongMessage: boolean;
  forwardHeartbeatMessage: boolean;
  interval: number;
  toleranceTimeout: number;
  heartbeatDebounce: number | null;
  doPing: (send: (data: any) => void) => void;
  doCheckPong: (event: MessageEvent) => boolean;
  doCheckHeartbeat: (event: MessageEvent) => boolean;
}

export interface PersistentWebsocketConnectionLogger {
  error: (message: string) => void;
  debug: (message: string) => void;
}

export type ConnectionLostReason = 'pong-or-heartbeat-timeout' | 'closed-unexpectedly' | 'error';

export interface ConnectionLostEvent {
  reason: ConnectionLostReason;
  error?: string;
  preventDefault: () => void;
}

export interface PersistentWebsocketConnectionKeepAlive {
  enabled: boolean;
  interval: number;
  doKeepAlive: (send: (data: any) => void) => void;
}

export interface PersistentWebsocketConnectionReconnectionBackoff {
  enabled: boolean;
  backoffOffset: number;
  backoffIncrement: number;
  maxBackedOffTimeout: number;
  backoffIncrementResetTimeout: number;
}

export interface PersistentWebsocketConnectionOptions {
  url: string;
  onData: (data: string) => void;
  onInitialConnectionEstablished: (send: (data: any) => void) => void;
  onReconnect: (send: (data: any) => void) => void;
  onReconnecting?: (tries: number) => void;
  onConnecting?: (connection: WebSocket) => void;
  onConnectionLost?: (event: ConnectionLostEvent) => void;
  reconnectTimeout?: number;
  reconnectBackoff?: PersistentWebsocketConnectionReconnectionBackoff;
  connectionCheckInterval?: number;
  logger?: Partial<PersistentWebsocketConnectionLogger>;
  pingPong?: Partial<PersistentWebsocketConnectionPingPongOptions>;
  keepAlive?: Partial<PersistentWebsocketConnectionKeepAlive>;
}
