import moment from 'moment';

import { OHLCVTimeUnitGranularity, OHLCVTimeUnit } from './types';

export function getGranularityDuration(granularity: OHLCVTimeUnitGranularity): number {
  switch (granularity) {
    case 'second':
      return Math.floor(1000);
    case 'minute':
      return Math.floor(1000 * 60);
    case 'hour':
      return Math.floor(1000 * 60 * 60);
    case 'day':
      return Math.floor(1000 * 60 * 60 * 24);
    case 'week':
      return Math.floor(1000 * 60 * 60 * 24 * 7);
    case 'month':
      return Math.floor(1000 * 60 * 60 * 24 * 30.44); // 30.44 is the average days in a month
    case 'year':
      return Math.floor(1000 * 60 * 60 * 24 * 365.25); // 365.25 is the average days in a year
  }
}

export function getTimeUnitDuration(timeUnit: OHLCVTimeUnit): number {
  return Math.round(getGranularityDuration(timeUnit.granularity) * timeUnit.period);
}

export function startOfGranularity(timestamp: number, granularity: OHLCVTimeUnitGranularity) {
  switch (granularity) {
    case 'second':
      return moment(timestamp).utc().startOf('second').valueOf();
    case 'minute':
      return moment(timestamp).utc().startOf('minute').valueOf();
    case 'hour':
      return moment(timestamp).utc().startOf('hour').valueOf();
    case 'day':
      return moment(timestamp).utc().startOf('day').valueOf();
    case 'week':
      return moment(timestamp).utc().startOf('isoWeek').valueOf();
    case 'month':
      return moment(timestamp).utc().startOf('month').valueOf();
    case 'year':
      return moment(timestamp).utc().startOf('year').valueOf();
  }
}

export function startOfTimeUnit(timestamp: number, timeUnit: OHLCVTimeUnit): number {
  const timeUnitDuration = getTimeUnitDuration(timeUnit);

  const intervalStartTimestamp = timeUnit.period === 1 ? timestamp : Math.floor(timestamp / timeUnitDuration) * timeUnitDuration;

  return startOfGranularity(intervalStartTimestamp, timeUnit.granularity);
}

export function startOfClosestTimeUnit(timestamp: number, timeUnit: OHLCVTimeUnit) {
  const timeUnitDuration = getTimeUnitDuration(timeUnit);

  const timestamp1 = timestamp;
  const timestamp2 = Math.round(timestamp + Math.round(timeUnitDuration / 2));

  const intervalStartTimestamp1 = timeUnit.period === 1 ? timestamp1 : Math.floor(timestamp1 / timeUnitDuration) * timeUnitDuration;
  const intervalStartTimestamp2 = timeUnit.period === 1 ? timestamp2 : Math.floor(timestamp2 / timeUnitDuration) * timeUnitDuration;

  const answer1 = startOfGranularity(intervalStartTimestamp1, timeUnit.granularity);
  const answer2 = startOfGranularity(intervalStartTimestamp2, timeUnit.granularity);

  const distance1 = Math.abs(answer1 - timestamp);
  const distance2 = Math.abs(answer2 - timestamp);

  return distance1 <= distance2 ? answer1 : answer2;
}

export function getTimeUnitStartsInRange(
  start: number,
  end: number,
  timeUnit: OHLCVTimeUnit,
  inclusion: 'inclusive' | 'exclusive' = 'inclusive',
) {
  const initialTimeUnitStart = startOfClosestTimeUnit(start, timeUnit) === start ? start : startOfTimeUnit(start, timeUnit);
  const finalTimeUnitStart = startOfClosestTimeUnit(end, timeUnit) === end ? end : startOfTimeUnit(end, timeUnit);

  const timeUnitDuration = getTimeUnitDuration(timeUnit);

  const result: number[] = [];

  if (inclusion === 'inclusive') {
    result.push(initialTimeUnitStart);
  }

  let currentTimeUnitStart = startOfClosestTimeUnit(Math.round(initialTimeUnitStart + timeUnitDuration), timeUnit);

  while (currentTimeUnitStart < finalTimeUnitStart) {
    if (result.length === 0 || currentTimeUnitStart !== result[result.length - 1]) {
      result.push(currentTimeUnitStart);
    }

    const nextTimeUnit = startOfClosestTimeUnit(Math.round(currentTimeUnitStart + timeUnitDuration), timeUnit);

    // This prevents cases where there are no intervals between start and end and the closets time unit stays the same
    if (nextTimeUnit === currentTimeUnitStart) {
      break;
    }

    currentTimeUnitStart = nextTimeUnit;
  }

  if (inclusion === 'inclusive') {
    result.push(finalTimeUnitStart);
  }

  return result;
}
