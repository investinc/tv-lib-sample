import { TypedEmitter } from 'tiny-typed-emitter';
import AVLTree from 'avl';

import { OHLCVTimeUnit, OHLCVDataItem } from './types';
import { startOfTimeUnit, getTimeUnitStartsInRange } from './utils';

export interface OHLCVAggregatorRecordLimitRetentionOptions {
  method: 'record-limit';
  retentionCount: number;
}

export interface OHLCVAggregatorTimeWindowRetentionOptions {
  method: 'time-window';
  retentionWindow: number;
}

export type OHLCVAggregatorRetentionOptions = OHLCVAggregatorRecordLimitRetentionOptions | OHLCVAggregatorTimeWindowRetentionOptions;

export interface OHLCVAggregatorOptions {
  timeUnit: OHLCVTimeUnit;
  enableHistory: boolean;
  autofillGaps?: boolean;
  lastKnowClosingPrice?: number;
  retention?: OHLCVAggregatorRetentionOptions;
}

export interface OHLCVLiveAggregatorEvents {
  'update': (lastBar: OHLCVDataItem) => void;
  'history-set': () => void;
  'history-reset': () => void;
}

export class OHLCVLiveAggregator extends TypedEmitter<OHLCVLiveAggregatorEvents> {
  private _timeUnit: OHLCVTimeUnit;
  private _retention: OHLCVAggregatorRetentionOptions | null;

  private _historyEnabled: boolean;
  private _autoGapFillEnabled: boolean;

  private _historyLoaded = false;

  private _preHistoryUpdateBuffer: OHLCVDataItem[] | null = null;

  private _initialLastKnowClosingPrice: number | null;
  private _data = new AVLTree<number, OHLCVDataItem>(undefined, true);

  public get timeUnit() {
    return this._timeUnit;
  }

  public get historyEnabled() {
    return this._historyEnabled;
  }

  public get autoGapFillEnabled() {
    return this._autoGapFillEnabled;
  }

  public get historyLoaded() {
    return this._historyLoaded;
  }

  constructor(options: OHLCVAggregatorOptions) {
    super();

    this._timeUnit = options.timeUnit;
    this._retention = options.retention || null;

    this._historyEnabled = options.enableHistory;
    this._autoGapFillEnabled = options.autofillGaps ?? false;

    this._initialLastKnowClosingPrice = typeof options.lastKnowClosingPrice === 'number' ? options.lastKnowClosingPrice : null;
  }

  private retain() {
    if (!this._retention) {
      return;
    }

    if (this._retention.method === 'record-limit' && this._data.size > this._retention.retentionCount) {
      while (this._data.size > 0 && this._data.size > this._retention.retentionCount) {
        this._data.pop();
      }
    }

    if (this._retention.method === 'time-window') {
      const minimumAllowedTimestamp = Date.now() - this._retention.retentionWindow;

      while (this._data.size > 0 && (this._data.min() as number) < minimumAllowedTimestamp) {
        this._data.pop();
      }
    }
  }

  private applyItem(update: OHLCVDataItem) {
    const targetTimeUnit = startOfTimeUnit(update.timeUnitStart, this._timeUnit);

    const old = this._data.find(targetTimeUnit);

    let inserted;

    if (old && old.data) {
      // The update should only ever be applied if the it's time_close
      // is greater than or equal to the current time_close of the target time_unit.
      if (update.timeClose >= old.data.timeClose) {
        this._data.remove(targetTimeUnit);

        inserted = {
          timeUnitStart: targetTimeUnit,
          timeOpen: old.data.timeOpen,
          timeClose: update.timeClose,
          open: old.data.open,
          high: Math.max(old.data.high, update.high),
          low: Math.min(old.data.low, update.low),
          close: update.close,
          volume: old.data.volume + update.volume,
        };

        this._data.insert(targetTimeUnit, inserted);
      } else {
        inserted = old.data;
      }
    } else {
      inserted = {
        timeUnitStart: targetTimeUnit,
        timeOpen: update.timeOpen,
        timeClose: update.timeClose,
        open: update.open,
        high: update.high,
        low: update.low,
        close: update.close,
        volume: update.volume,
      };

      this._data.insert(targetTimeUnit, inserted);
    }

    return inserted;
  }

  public resetHistory = () => {
    if (!this._historyEnabled) {
      throw new Error('History is disabled');
    }

    this._historyLoaded = false;

    this.emit('history-reset');
  };

  public setHistory = (history: OHLCVDataItem[]) => {
    if (!this._historyEnabled) {
      throw new Error('History is disabled');
    }

    if (this._historyLoaded) {
      throw new Error('History cannot be loaded more than once, to reset the history first call resetHistory then setHistory');
    }

    this._historyLoaded = true;

    this._data.clear();

    for (const historyItem of history) {
      this.applyItem(historyItem);
    }

    if (this._preHistoryUpdateBuffer && this._preHistoryUpdateBuffer.length > 0) {
      for (const bufferedItem of this._preHistoryUpdateBuffer) {
        this.applyItem(bufferedItem);
      }
    }

    this._preHistoryUpdateBuffer = null;

    this.retain();

    this.emit('history-set');
  };

  public applyUpdate = (update: OHLCVDataItem) => {
    if (this._historyEnabled && !this._historyLoaded) {
      if (!this._preHistoryUpdateBuffer) {
        this._preHistoryUpdateBuffer = [];
      }

      this._preHistoryUpdateBuffer.push(update);

      return null;
    }

    const inserted = this.applyItem(update);

    this.retain();

    this.emit('update', inserted);

    return inserted;
  };

  public getHasData = () => {
    return this._data.size !== 0;
  };

  public getData = () => {
    if (this._autoGapFillEnabled) {
      const result: OHLCVDataItem[] = [];

      let lastTimeUnitStart: number | null = null;
      let lastPrice: number | null = null;

      this._data.forEach(node => {
        const item = node.data;

        if (item) {
          if (lastTimeUnitStart !== null && lastPrice !== null) {
            const gaps = getTimeUnitStartsInRange(lastTimeUnitStart, item.timeUnitStart, this._timeUnit, 'exclusive');

            for (const gap of gaps) {
              result.push({
                timeUnitStart: gap,
                timeOpen: gap,
                timeClose: gap,
                open: lastPrice,
                high: lastPrice,
                low: lastPrice,
                close: lastPrice,
                volume: 0,
              });
            }
          }

          result.push(item);

          lastTimeUnitStart = item.timeUnitStart;
          lastPrice = item.close;
        }
      });

      const currentTimeUnitStart = startOfTimeUnit(Date.now(), this._timeUnit);
      const last = result[result.length - 1];

      if (last && last.timeUnitStart !== currentTimeUnitStart) {
        const gaps = getTimeUnitStartsInRange(last.timeUnitStart, currentTimeUnitStart, this._timeUnit);

        for (const gap of gaps) {
          if (gap !== last.timeUnitStart) {
            result.push({
              timeUnitStart: gap,
              timeOpen: gap,
              timeClose: gap,
              open: last.close,
              high: last.close,
              low: last.close,
              close: last.close,
              volume: 0,
            });
          }
        }
      }

      return result;
    }

    return this._data.values();
  };

  public getMostRecentOHLCVItem = (): OHLCVDataItem | null => {
    return (this._data.size > 0 && this._data.maxNode()?.data) || null;
  };

  public getCurrentTimeUnitOHLCVItem = (): OHLCVDataItem | null => {
    const lastBar = this._data.size > 0 && this._data.maxNode()?.data;

    const currentTimeUnitStart = startOfTimeUnit(Date.now(), this._timeUnit);

    if (lastBar) {
      if (lastBar.timeUnitStart === currentTimeUnitStart) {
        return lastBar;
      } else {
        return {
          timeUnitStart: currentTimeUnitStart,
          timeOpen: currentTimeUnitStart,
          timeClose: currentTimeUnitStart,
          open: lastBar.close,
          high: lastBar.close,
          low: lastBar.close,
          close: lastBar.close,
          volume: 0,
        };
      }
    } else if (this._initialLastKnowClosingPrice !== null) {
      return {
        timeUnitStart: currentTimeUnitStart,
        timeOpen: currentTimeUnitStart,
        timeClose: currentTimeUnitStart,
        open: this._initialLastKnowClosingPrice,
        high: this._initialLastKnowClosingPrice,
        low: this._initialLastKnowClosingPrice,
        close: this._initialLastKnowClosingPrice,
        volume: 0,
      };
    }

    return null;
  };

  public getLastKnownClosingPrice = () => {
    if (this._data.size > 0) {
      const lastEntry = this._data.maxNode()?.data;

      if (lastEntry) {
        return lastEntry.close;
      }
    }

    return this._initialLastKnowClosingPrice;
  };
}
