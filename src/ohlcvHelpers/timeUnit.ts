import { OHLCVTimeUnit, OHLCVTimeUnitGranularity } from './types';

export type OHLCVTimeUnitStringFormats = 'default' | 'trading-view' | 'coin-api';

export const ohlcvTimeUnitGranularities: readonly OHLCVTimeUnitGranularity[] = Object.freeze([
  'second',
  'minute',
  'hour',
  'day',
  'week',
  'month',
  'year',
]);

export function isOHLCVTimeUnitValid(timeUnit: any): timeUnit is OHLCVTimeUnit {
  if (!timeUnit) {
    return false;
  }

  if (typeof timeUnit.granularity !== 'string' || typeof timeUnit.period !== 'number') {
    return false;
  }

  if (Number.isNaN(timeUnit.period) || !Number.isFinite(timeUnit.period) || timeUnit.period <= 0) {
    return false;
  }

  if (!ohlcvTimeUnitGranularities.includes(timeUnit.granularity)) {
    return false;
  }

  return true;
}

export function ohlcvTimeUnitToString(timeUnit: OHLCVTimeUnit, format: OHLCVTimeUnitStringFormats = 'default'): string {
  switch (format) {
    case 'default':
      return `${timeUnit.period}_${timeUnit.granularity.toLowerCase()}`;
    case 'trading-view':
      switch (timeUnit.granularity) {
        case 'second':
          return timeUnit.period === 1 ? 'S' : `${timeUnit.period}S`;
        case 'minute':
          return `${timeUnit.period}`;
        case 'hour':
          return `${timeUnit.period * 60}`;
        case 'day':
          return timeUnit.period === 1 ? 'D' : `${timeUnit.period}D`;
        case 'week':
          return timeUnit.period === 1 ? 'W' : `${timeUnit.period}W`;
        case 'month':
          return timeUnit.period === 1 ? 'M' : `${timeUnit.period}M`;
        case 'year':
          return `${timeUnit.period * 12}M`;
      }

      throw new Error('Invalid granularity');
    case 'coin-api':
      switch (timeUnit.granularity) {
        case 'second':
          return `${timeUnit.period}SEC`;
        case 'minute':
          return `${timeUnit.period}MIN`;
        case 'hour':
          return `${timeUnit.period}HRS`;
        case 'day':
          return `${timeUnit.period}DAY`;
        case 'week':
          return `${timeUnit.period * 7}DAY`;
        case 'month':
          return `${timeUnit.period}MTH`;
        case 'year':
          return `${timeUnit.period}YRS`;
      }

      throw new Error('Invalid granularity');
  }
}

export function ohlcvTimeUnitToDisplayString(timeUnit: OHLCVTimeUnit) {
  const titleText = `${timeUnit.granularity[0].toUpperCase()}${timeUnit.granularity.substring(1)}`;

  return `${timeUnit.period} ${titleText}${timeUnit.period > 1 ? 's' : ''}`;
}

export function ohlcvTimeUnitFromString(str: string | undefined | null | false): OHLCVTimeUnit | null {
  if (!str || typeof str !== 'string') {
    return null;
  }

  // Own Format
  if (str.trim().includes('_')) {
    const [num, granularity] = str.trim().split('_');

    const period = Number(num);

    return { period, granularity: granularity.toLowerCase() as OHLCVTimeUnitGranularity };
  }

  // Trading View chart format
  const tradingViewMatch = /^(\d*)([sSdDwWmM])?$/gm.exec(str.trim());

  if (tradingViewMatch) {
    const [, periodString, granularity] = tradingViewMatch;

    const period = Number(periodString.trim() || '1');

    if (Number.isNaN(period) || period <= 0) {
      return null;
    }

    switch ((granularity || '').toUpperCase()) {
      case 'S':
        return { period, granularity: 'second' };
      case 'D':
        return { period, granularity: 'day' };
      case 'W':
        return { period, granularity: 'week' };
      case 'M':
        if (period >= 12 && period % 12 === 0) {
          return { period: Math.round(period / 12), granularity: 'year' };
        }

        return { period, granularity: 'month' };
    }

    if (period >= 60 && period % 60 === 0) {
      return { period: Math.round(period / 60), granularity: 'hour' };
    }

    return { period, granularity: 'minute' };
  }

  // CoinAPI format
  const coinAPIMatch = /^(\d+)([a-zA-Z]{3})$/gm.exec(str.trim());

  if (coinAPIMatch) {
    const [, periodString, granularity] = coinAPIMatch;

    const period = Number(periodString);

    if (Number.isNaN(period) || period <= 0) {
      return null;
    }

    switch ((granularity || '').toUpperCase()) {
      case 'SEC':
        return { period, granularity: 'second' };
      case 'MIN':
        return { period, granularity: 'minute' };
      case 'HRS':
        return { period, granularity: 'hour' };
      case 'DAY':
        if (period >= 7 && period % 7 === 0) {
          return { period: Math.round(period / 7), granularity: 'week' };
        }

        return { period, granularity: 'day' };
      case 'MTH':
        return { period, granularity: 'month' };
      case 'YRS':
        return { period, granularity: 'year' };
    }
  }

  return null;
}

export function convertTimeUnitStringFormat(timeUnitStr: string, toFormat?: OHLCVTimeUnitStringFormats) {
  const timeUnit = timeUnitStr ? ohlcvTimeUnitFromString(timeUnitStr) : null;

  return timeUnit ? ohlcvTimeUnitToString(timeUnit, toFormat) : null;
}
