export type OHLCVTimeUnitGranularity = 'second' | 'minute' | 'hour' | 'day' | 'week' | 'month' | 'year';

export interface OHLCVTimeUnit {
  granularity: OHLCVTimeUnitGranularity;
  period: number;
}

export interface OHLCVDataItem {
  timeUnitStart: number;
  timeOpen: number;
  timeClose: number;
  open: number;
  high: number;
  close: number;
  low: number;
  volume: number;
}
