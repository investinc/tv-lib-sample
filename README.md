# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Setup and Run tv-lib-sample ###

Setup Node Version Manager (nvm) so you can run node at different versions

    node -v

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

    load nvm

    export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

    npm install webpack-dev-server -g // Get web pack server
    npm install -D webpack-cli
    nvm install 16 
    nvm use 16

    npm start
