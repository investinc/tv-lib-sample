const { resolve, join } = require('path');
const compact = require('lodash/compact');

const { EnvironmentPlugin } = require('webpack');
const ForkTsCheckerPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const PROJECT_ROOT = resolve(__dirname);
const SRC_FOLDER = resolve(PROJECT_ROOT, 'src');
const BUILD_FOLDER = resolve(__dirname, 'build');

module.exports = {
  entry: resolve(SRC_FOLDER, 'index.tsx'),
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [{ loader: 'ts-loader', options: { transpileOnly: true, experimentalWatchApi: true } }],
      },
      {
        test: /\.(png|img|svg|woff|woff2|otf|ttf|eot)$/,
        loader: 'url-loader',
      },
    ],
  },
  plugins: compact([
    new ForkTsCheckerPlugin({ typescript: { configFile: resolve(PROJECT_ROOT, 'tsconfig.json') } }),
    new EnvironmentPlugin({ NODE_ENV: 'development' }),
    new HtmlPlugin({
      title: 'Aurox PineScript Tester',
      inject: true,
      hash: true,
      template: resolve(SRC_FOLDER, 'index.html'),
      filename: 'index.html',
    }),
    new CopyPlugin({ patterns: [{ from: join(PROJECT_ROOT, 'static'), to: join(BUILD_FOLDER) }] }),
  ]),
  output: {
    pathinfo: false,
    path: BUILD_FOLDER,
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    modules: [SRC_FOLDER, 'node_modules'],
  },
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: {
    static: BUILD_FOLDER,
  },
  watch: true,
};
